#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from setuptools import setup, find_packages


def package_files(directory):
    paths = []
    for (path, directories, filenames) in os.walk(directory):
        for filename in filenames:
            paths.append(os.path.join('..', path, filename))
    return paths

extra_files = package_files('anteater/template')

with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    include_package_data=True,
    install_requires=['PyYAML'],
    name='anteater',
    version='0.0.1',
    description='A build tool for Android projects',
    long_description=readme,
    author='Paulo Pinheiro',
    author_email='paulovictor.pinheiro@gmail.com',
    url='https://github.com/paulovap/anteater',
    license=license,
    packages=find_packages(exclude=('tests', 'docs')),
    package_data={'': extra_files},
    entry_points={
        'console_scripts': [
            'anteater=anteater.anteater:main',
        ],
    })

