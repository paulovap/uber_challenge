# -*- coding: utf-8 -*-

"""
anteater.command_compile
~~~~~~~~~~~~~~~~~~~~~~~~
Generates the R.java class and compile all java classes into a DEX file.
"""

import os
import run_tool
import config
from command import Command
import file_utils


class CommandCompile(Command):

    def name(self):
        return "compile"

    def require_project_folder(self):
        return True

    def validate_args(self, args):
        pass

    def run(self, args):
        # remove dex if exists
        file_utils.rm(os.path.join(args.project_config.bin_path, config.DEX_NAME))
        # remove R
        file_utils.rm(os.path.join(args.project_config.src_path, "R.java"))

        android_jar = os.path.join(args.project_config.tools.android_home,
                                   "platforms", args.project_config.tools.target_sdk, "android.jar")
        jars = filter(lambda x: x.endswith(".jar"), os.listdir(args.project_config.libs_path) + [android_jar])
        resources = [args.project_config.res_path]
        run_tool.aapt_generate_r(args.project_config, resources)
        run_tool.javac(args.project_config, jars, file_utils.scan_filter(args.project_config.src_path, ".java"))
        run_tool.dx(args.project_config)

    def add_parser(self, sub_parser):
        parser = sub_parser.add_parser(self.name(), help='compile the source code, generating the dex files')
        parser.set_defaults(func=self.execute)

