# -*- coding: utf-8 -*-

"""
run_tool
~~~~~~~~~~~~~~~~~~~~~~~
Collection of helper functions to run android and java tools.
"""

from subprocess import Popen, PIPE
import logging
import os
import sys

logger = logging.getLogger("anteater")
verbose = False


class RunToolException(Exception):
    pass


def set_verbose(is_verbose):
    global verbose
    verbose = is_verbose


def adb_install(project_config):
    apk_path = os.path.join(project_config.bin_path, "debug-final.apk")
    out = run(project_config.tools.adb, ["install", "-r", apk_path])
    if verbose:
        sys.stdout.write(out)
    logger.info("%s installed on active device", apk_path)


def android_list_targets(tools_config):
    out = run(tools_config.android, ["list", "targets", "-c"])
    return out.replace("\r", "").strip().split("\n")


def android_create(project_config, app_name, package, target_sdk, path):
    out = run(project_config.tools.android, [
        "create", "project", "-n", app_name,
        "-a", "MainActivity", "-k", package,
        "-p", path, "-t", target_sdk
    ])
    if verbose:
        sys.stdout.write(out)
    logger.info("project %s created at %s", app_name, path)


def zipalign(project_config):
    out = run(project_config.tools.zipalign, [
        "-v", "4",
        os.path.join(project_config.bin_path, "debug-unaligned.apk"),
        os.path.join(project_config.bin_path, "debug-final.apk")])
    if verbose:
        sys.stdout.write(out)
    logger.info("generated %s", "debug-final.apk")


def jarsigner(project_config, key_path):
    out = run(project_config.tools.jarsigner, [
        "-verbose",
        "-keystore", key_path,
        "-storepass", "android",
        "-keypass", "android",
        os.path.join(project_config.bin_path, "debug-unaligned.apk"),
        "androiddebugkey"
    ])
    if verbose:
        sys.stdout.write(out)
    logger.info("debug-unaligned.apk signed")


def dx(project_config):
    if not os.path.exists(project_config.bin_path):
        os.makedirs(project_config.bin_path)
    out = run(project_config.tools.dx, [
        "--dex",
        "--debug",
        "--multi-dex",
        "--output", project_config.bin_path,
        project_config.obj_path,
        project_config.libs_path])
    if verbose:
        sys.stdout.write(out)
    logger.info("class.dex generated")


def javac(project_config, class_paths, java_files):
    if not os.path.exists(project_config.obj_path):
        os.makedirs(project_config.obj_path)
    out = run(project_config.tools.javac, [
        "-verbose", "-g", "-XDuseUnsharedTable=true",
        "-source", "7",
        "-target", "7",
        "-d", project_config.obj_path,
        "-encoding", "UTF-8",
        "-bootclasspath", os.path.join(project_config.tools.android_home,
                                       "platforms", project_config.tools.target_sdk, "android.jar"),
        "-classpath", ":".join(class_paths),
        "-sourcepath", project_config.src_path] + java_files)
    if verbose:
        sys.stdout.write(out)
    logger.info("classes compiled")


def aapt_generate_apk(project_config, resource_folders):
    if not os.path.exists(project_config.bin_path):
        os.makedirs(project_config.bin_path)
    args = [
        "package", "-v", "-f", "--auto-add-overlay",
        "-S", project_config.res_path,
        "-M", os.path.join(project_config.home_path, "AndroidManifest.xml"),
        "-I", os.path.join(project_config.tools.android_home,
                           "platforms", project_config.tools.target_sdk, "android.jar"),
        "-F", os.path.join(project_config.bin_path, "debug-unaligned.apk"),
    ]
    for resource_folder in resource_folders:
        args += ["-S", resource_folder]
    args.append(project_config.bin_path)
    out = run(project_config.tools.aapt, args)
    if verbose:
        sys.stdout.write(out)
    logger.info("debug-unaligned.apk generated")


def aapt_generate_r(project_config, resource_folders):
    args = [
        "package", "-v", "--auto-add-overlay", "-f", "-m",
        "-J", project_config.src_path,
        "-M", os.path.join(project_config.home_path, "AndroidManifest.xml"),
        "-I", os.path.join(project_config.tools.android_home,
                           "platforms", project_config.tools.target_sdk, "android.jar"),
    ]
    for resource_folder in resource_folders:
        args += ["-S", resource_folder]
    out = run(project_config.tools.aapt, args)
    if verbose:
        sys.stdout.write(out)
    logger.info("R.java generated")


def run(cmd, args):
    logger.debug('running program "%s"', cmd)
    logger.debug('with arguments %r', args)
    p = Popen([cmd] + args, stdout=PIPE, stderr=PIPE)
    (out, err) = p.communicate()
    if p.returncode != 0:
        raise RunToolException("External tool {} exited with {}, error message:{} {}"
                               .format(cmd, p.returncode, os.linesep, err))
    logger.debug('program "%s" finished', cmd)
    return out
