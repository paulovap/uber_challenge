# -*- coding: utf-8 -*-

"""
anteater.command_new
~~~~~~~~~~~~~~~
This module provides implementation for the "new" command. "new" command create an new android project based on
the template project.
"""

import os
import re
import config
import run_tool
import file_utils
from command import (Command, InvalidArgumentException)


class CommandNew(Command):

    def name(self):
        return "new"

    def require_project_folder(self):
        return False

    def validate_args(self, args):
        # we assume it is safe to load tools configuration
        # because it is checked in the program's main function
        tools = config.ToolsConfig(None, None, None)
        # and get a list of targets to check if given target exists
        targets = run_tool.android_list_targets(tools)
        # package
        if not re.match(r'^[a-zA-Z_\$][\w\$]*(?:\.[a-zA-Z_\$][\w\$]*)+$', args.package):
            raise InvalidArgumentException(("Invalid package name."
                                            "See: http://docs.oracle.com/javase/tutorial/java/package/namingpkgs.html"))
        # app_name
        if not re.match(r'\w[\w\s]{0,12}', args.app_name):
            raise InvalidArgumentException("Invalid app name. Maximum size is 12 characters (including spaces)")
        # directory
        if args.directory and os.path.exists(args.directory):
                raise InvalidArgumentException("Directory {} already exists".format(args.directory))
        # target_sdk
        if not args.target_sdk:
            vars(args)["target_sdk"] = targets[-1]
        elif args.target_sdk not in targets:
            raise InvalidArgumentException("Target sdk {} not found".format(args.target_sdk))

    def run(self, args):
        # we copy our config file from template to our project folder
        # applying template transformation
        config_file_out = os.path.join(args.directory, config.CONFIG_FILENAME)
        file_utils.cp_and_apply_template(os.path.join(file_utils.PATH, config.CONFIG_FILENAME),
                                         config_file_out, vars(args))
        project_config = config.read_project_config(args.directory)
        # we run android tool to create a new project from scratch
        run_tool.android_create(project_config, args.app_name, args.package, args.target_sdk, args.directory)
        self._remove_unused_project_file(args.directory)

    def add_parser(self, sub_parser):
        parser = sub_parser.add_parser(self.name(), help='creates a skeleton for a new Android project')
        parser.add_argument('--app_name', help='name that will be shown in your app (i.e SampleApp)', type=str,
                            default="SampleApp")
        parser.add_argument('--package',
                            help='root directory of the project to be scanned (i.e. org.anteater.sampleapp)',
                            type=str, default="org.anteater.sampleapp")
        parser.add_argument('--target_sdk', help='Android target sdk (i.e. android-23). If no target provided'
                                                 ' the system will use the latest available', type=str, default=None)
        parser.add_argument('directory', help='name of a new directory that will be generated')
        parser.set_defaults(func=self.execute)

    def _remove_unused_project_file(self, project_path):
        filenames = ["build.xml", "local.properties", "ant.properties", "project.properties"]
        for file_path in map(lambda f: os.path.join(project_path, f), filenames):
            file_utils.rm(file_path)
