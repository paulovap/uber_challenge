# -*- coding: utf-8 -*-

"""
anteater.command_apk
~~~~~~~~~~~~~~~
"""

import os
import run_tool
import command_compile
from command import Command, InvalidArgumentException
import file_utils
from config import DEBUG_KEYSTORE


class CommandPack(Command):

    def name(self):
        return "pack"

    def require_project_folder(self):
        return True

    def validate_args(self, args):
        dex_file = os.path.join(args.project_config.bin_path, "classes.dex")
        if not os.path.exists(dex_file):
            raise InvalidArgumentException('{} not found! please run command "{}" before generate an apk'
                                           .format(dex_file, command_compile.CommandCompile().name()))

    def run(self, args):
        # cleanup first
        file_utils.rm(os.path.join(args.project_config.bin_path, "debug-final.apk"))
        file_utils.rm(os.path.join(args.project_config.bin_path, "debug-unaligned.apk"))

        run_tool.aapt_generate_apk(args.project_config, [args.project_config.res_path])
        run_tool.jarsigner(args.project_config, os.path.join(file_utils.PATH, DEBUG_KEYSTORE))
        run_tool.zipalign(args.project_config)

    def add_parser(self, sub_parser):
        parser = sub_parser.add_parser(self.name(), help='generate the apk file and sign it with debug keystore')
        parser.set_defaults(func=self.execute)
