# -*- coding: utf-8 -*-

"""
anteater.command_deploy
~~~~~~~~~~~~~~~~~~~~~~~
Install the signed apk into a active Android device or emulator.
"""

import os
import config
import run_tool
import command
import command_pack


class CommandDeploy(command.Command):

    def name(self):
        return "deploy"

    def require_project_folder(self):
        return True

    def validate_args(self, args):
        apk_file = os.path.join(args.project_config.bin_path, config.APK_FINAL_NAME)
        if not os.path.exists(apk_file):
            raise command.InvalidArgumentException('{} not found! please run command "{}" before deploying to a device'
                                                   .format(apk_file, command_pack.CommandPack().name()))

    def run(self, args):
        run_tool.adb_install(args.project_config)

    def add_parser(self, sub_parser):
        parser = sub_parser.add_parser(self.name(), help="deploy the final apk into a running "
                                                         "android device or emulator")
        parser.set_defaults(func=self.execute)

