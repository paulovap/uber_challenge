#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import logging
import os
import sys
import config
import run_tool
import command_new
import command_compile
import command_pack
import command_deploy

available_commands = [
    command_new.CommandNew(),
    command_compile.CommandCompile(),
    command_pack.CommandPack(),
    command_deploy.CommandDeploy()]


def main():
    parser = argparse.ArgumentParser(prog="anteater", description="An Android cli build system")
    parser.add_argument("-v", "--verbose", help="enabled verbose mode", action="store_true")
    sub_parser = parser.add_subparsers(title="command", help="for more info type command --help")

    for cmd in available_commands:
        cmd.add_parser(sub_parser)

    args = parser.parse_args()
    run_tool.set_verbose(args.verbose)
    config.configure_log(args.verbose)

    try:
        config.verify_java_jdk(os.environ.get("JAVA_HOME"))
        config.verify_android_sdk(os.environ.get("ANDROID_HOME"))
    except config.ProjectConfigException as err:
        logging.error(err.message)
        sys.exit(1)

    args.func(args)

if __name__ == '__main__':
    main()
