# -*- coding: utf-8 -*-

"""
anteater.command
~~~~~~~~~~~~~~~~
This module provides the base class for creating anteater commands.
"""

import abc
import sys
import logging
import config
import os
import subprocess
import yaml
from config import ProjectConfigException
from run_tool import RunToolException

logger = logging.getLogger("anteater")


class RunHookException(Exception):
    pass


class InvalidArgumentException(Exception):
    pass


class CommandExecutionException(Exception):
    pass


class Command(object):
    """Abstract class used to create new commands"""
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def name(self):
        """Command name that will be used in the CLI"""
        pass

    @abc.abstractmethod
    def require_project_folder(self):
        """Should return true if the command needs to run on the project folder. For those cases,
           a project_config object will be added in the args namespace.
        """
        pass

    @abc.abstractmethod
    def validate_args(self, args):
        """Validate arguments received from argv for the given command"""
        return

    @abc.abstractmethod
    def run(self, args):
        """A command should implement this method to run. This method will be called after validate_args."""
        return

    @abc.abstractmethod
    def add_parser(self, sub_parser):
        """Add command configuration to the argparser object."""
        return

    def execute(self, args):
        logger.debug('selected command "%s"', self.name())
        try:
            config.ToolsConfig(None, None, None)
            hooks_before = None
            hooks_after = None
            if self.require_project_folder():
                hooks = None
                logger.debug("checking tools")
                args.project_config = config.read_project_config(os.getcwd())
                if args.project_config.hooks:
                    hooks = args.project_config.hooks.get(self.name())
                if hooks:
                    hooks_before = hooks.get("before")
                    hooks_after = hooks.get("after")
                logger.debug("android sdk and java jdk found!")

            logger.debug('validating arguments for "%s"', self.name())
            self.validate_args(args)
            logger.debug("arguments validated!")

            if hooks_before:
                logging.debug("running before hooks:")
                self._run_hooks(hooks_before)
                logging.debug("before hooks completed!")

            logger.debug('running "%s"', self.name())
            self.run(args)
            logger.debug('"%s" finished', self.name())

            if hooks_after:
                logging.debug("running after hooks:")
                self._run_hooks(hooks_after)
                logging.debug("after hooks completed!")
        except yaml.YAMLError, exc:
            if hasattr(exc, 'problem_mark'):
                mark = exc.problem_mark
                logger.error("error in configuration file at line %s column %s", mark.line+1, mark.column+1)
        except (ProjectConfigException, InvalidArgumentException,
                CommandExecutionException, RunHookException, RunToolException) as err:
            logger.error("error: {}".format(err.message))
            sys.exit(1)
        except Exception as err:
            logger.exception(err)
            sys.exit(1)

    def _run_hooks(self, hooks):
        for hook in hooks:
            cmd = hook.get("cmd")
            try:
                logger.debug("running hook %s", cmd)
                subprocess.call(cmd, shell=True)
            except:
                raise RunHookException("Hook {} failed.".format(cmd))


