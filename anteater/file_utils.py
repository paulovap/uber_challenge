# -*- coding: utf-8 -*-

"""
file_utils
~~~~~~~~~~~~~~~~~~~~~~~
Collection of helper functions to handle files
"""

from string import Template
import os
import shutil
import pkg_resources
import logging
from zipfile import ZipFile

PATH = pkg_resources.resource_filename(__name__, "template")

logger = logging.getLogger("anteater")


def rm(src):
    if os.path.exists(src):
        logging.debug("Removed file %s", src)
        os.remove(src)


def mv(src, dst):
    logger.debug("mv %s -> %s", src, dst)
    shutil.move(src, dst)


def cp_tree_and_apply_template(src, dst, mapping, file_filters):

    for root, dirs, files in os.walk(src):
        for f in files:
            templated_file = False
            src_file = os.path.abspath(os.path.join(root, f))
            dst_file = src_file.replace(src, dst)
            dst_directory = os.path.dirname(dst_file)
            if not os.path.exists(dst_directory):
                os.makedirs(dst_directory)
            for ffilter in file_filters:
                if dst_file.endswith(ffilter):
                    cp_and_apply_template(src_file, dst_file, mapping)
                    templated_file = True
                    break
            if not templated_file:
                logger.debug("cp %s -> %s", os.path.basename(src_file), dst_file)
                shutil.copy(src_file, dst_file)


def cp_and_apply_template(src, dst, mapping):
    logger.debug("cp_apply %s -> %s", os.path.basename(src), dst)
    folder = os.path.dirname(dst)
    if not os.path.exists(folder):
        os.makedirs(folder)
    with open(src, "r") as src_file:
        with open(dst, "w") as dst_file:
            for line in src_file:
                new_line = Template(line).safe_substitute(mapping)
                dst_file.write(new_line)


def extract_aar(filepath, dst):
    logger.debug("extracting %s -> %s", filepath, dst)
    with ZipFile(filepath, 'r') as z:
        z.extractall(os.path.join(dst, os.path.basename(filepath)))


def scan_filter(basedir, extension):
    out = []
    for root, dirs, files in os.walk(basedir):
        for f in files:
            if f.endswith(extension):
                out.append(os.path.join(root, f))
    return out
