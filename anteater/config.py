# -*- coding: utf-8 -*-

"""
config
~~~~~~~~~~~~~~~~~~~~~~~
Loads project configuration from YAML file.
"""

import yaml
import os
import logging

DEBUG_KEYSTORE = "debug.keystore"
CONFIG_FILENAME = "build.yaml"
APK_FINAL_NAME = "debug-final.apk"
DEX_NAME = "classes.dex"

logger = logging.getLogger("anteater")

android_tools = [
    ("build-tools", ["aapt", "dx", "zipalign"]),
    ("platform-tools", ["adb"]),
    ("tools", ["emulator", "android"])]

java_tools = ["keytool", "javac", "jarsigner"]


class ProjectConfigException(Exception):
    pass


def configure_log(verbose):
    if verbose:
        log_level = logging.DEBUG
        formatter = '%(asctime)s %(levelname)s: %(message)s'
    else:
        log_level = logging.INFO
        formatter = '%(message)s'
    logging.basicConfig(format=formatter, level=log_level, datefmt="%H:%M:%S")


def verify_android_sdk(path):
    if not path or not os.path.exists(path):
        raise ProjectConfigException(("Android SDK not found. Please provide a path to Android SDK in "
                                      "ANDROID_HOME environment variable."))


def verify_java_jdk(path):
    if not path or not os.path.exists(path):
        # we fallback to $PATH and check if we find javac program
        javac_path = which("javac.exe" if os.name == "nt" else "javac")
        if javac_path:
            os.environ["JAVA_HOME"] = os.path.dirname(os.path.dirname(javac_path))
        else:
            raise ProjectConfigException(("JDK not found. Please provide a path to JDK in "
                                          "JAVA_HOME environment variable."))


def read_project_config(project_path):
    config_file = os.path.join(project_path, CONFIG_FILENAME)
    if not os.path.exists(config_file):
        raise ProjectConfigException(("Project file ({}) not found! You must run this command "
                                      "in a valid project folder").format(CONFIG_FILENAME))
    with open(config_file, "r") as f:
        config_dict = yaml.load(f)
        return ProjectConfig(config_dict)


def fetch_param(config_dict, key, default=None):
    return config_dict.get(key) or default


def which(program):
    fpath, fname = os.path.split(program)
    if fpath:
        if is_exe(program):
            return program
    else:
        for path in os.environ["PATH"].split(os.pathsep):
            path = path.strip('"')
            exe_file = os.path.join(path, program)
            if is_exe(exe_file):
                return exe_file

    return None


def is_exe(fpath):
    return os.path.isfile(fpath) and os.access(fpath, os.X_OK)


class ToolsConfig(object):

    def __init__(self, java_path, android_path, target_sdk=None):
        self.java_home = java_path or os.environ.get("JAVA_HOME")
        self.android_home = android_path or os.environ.get("ANDROID_HOME")
        self.target_sdk = target_sdk
        self.tools = java_tools
        verify_android_sdk(self.android_home)
        verify_java_jdk(self.java_home)
        for tool in java_tools:
            tool_with_extension = tool if os.name != "nt" else tool + ".exe"
            setattr(self, tool, os.path.join(self.java_home, "bin", tool_with_extension))
        for subfolder, tools in android_tools:
            self.tools += tools
            if subfolder == "build-tools":
                subfolder = os.path.join("build-tools",
                                         os.listdir(os.path.join(self.android_home, "build-tools"))[0])
            for tool in tools:
                tool_with_extension = tool
                if os.name == "nt":
                    tool_with_extension = tool + ".bat" if tool == "dx" or tool == "android" else tool + ".exe"
                self._add_android_tool(tool, subfolder, tool_with_extension)

    def __getitem__(self, item):
        return getattr(self, item)

    def _add_android_tool(self, tool, subfolder, tool_with_extension):
        setattr(self, tool, os.path.join(self.android_home, subfolder, tool_with_extension))

    def _validate_tools(self):
        for tool in self.tools:
            if not os.path.exists(tool):
                raise ProjectConfigException(("{} not found!"
                                              " Please verify that the android SDK and Java JDK is properly installed.")
                                             .format(tool))


class ProjectConfig(object):
    def __init__(self, config_dict):
        self.home_path = os.getcwd()
        self.libs_path = os.path.join(self.home_path, "libs")
        self.src_path = os.path.join(self.home_path, "src")
        self.res_path = os.path.join(self.home_path, "res")
        self.bin_path = os.path.join(self.home_path, "bin")
        self.obj_path = os.path.join(self.home_path, "obj")
        tools = config_dict.get("tools")
        self.tools = ToolsConfig(tools.get("java_path"), tools.get("android_path"), tools.get("target_sdk"))
        self.hooks = config_dict.get("hooks")
