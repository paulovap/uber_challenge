# -*- coding: utf-8 -*-

import os
import anteater.file_utils as file_utils
import unittest
import logging
import shutil
DATA_FOLDER = os.path.join(os.path.dirname(__file__), 'data')
logging.disable(logging.NOTSET)


class FileUtilsTestSuite(unittest.TestCase):
    """Basic test cases."""

    def tearDown(self):
        if os.path.exists("tmp"):
            shutil.rmtree("tmp")

    def test_cp_tree_and_apply_template_without_filter(self):
        d = {"name": "Jhon", "template": "some_string"}
        file_utils.cp_tree_and_apply_template(os.path.join("tests", "data", "file_utils"), "tmp", d, [])
        assert(os.path.exists("tmp"))
        assert(os.path.exists(os.path.join("tmp", "deep", "folder", "test.txt")))
        assert(os.path.exists(os.path.join("tmp", "deep", "folder", "test.xml")))
        xml = open(os.path.join("tmp", "deep", "folder", "test.xml"), "r")
        txt = open(os.path.join("tmp", "deep", "folder", "test.txt"), "r")
        assert("Jhon" not in xml.read())
        assert("${name}" in txt.read())

    def test_cp_tree_and_apply_template_with_filter(self):
        d = {"name": "Jhon", "template": "some_string"}
        file_utils.cp_tree_and_apply_template(os.path.join("tests", "data", "file_utils"), "tmp", d, [".xml"])
        assert(os.path.exists("tmp"))
        assert(os.path.exists(os.path.join("tmp", "deep", "folder", "test.txt")))
        assert(os.path.exists(os.path.join("tmp", "deep", "folder", "test.xml")))
        xml = open(os.path.join("tmp", "deep", "folder", "test.xml"), "r")
        txt = open(os.path.join("tmp", "deep", "folder", "test.txt"), "r")
        assert("Jhon" in xml.read())
        assert("${name}" in txt.read())


if __name__ == '__main__':\
    unittest.main()
