# -*- coding: utf-8 -*-

import os
import anteater.config as config
import unittest
import logging

DATA_FOLDER = os.path.join(os.path.dirname(__file__), 'data')
logging.disable(logging.NOTSET)


class ApplicationConfigTestSuite(unittest.TestCase):
    """Basic test cases."""

    def setUp(self):
        self.android_home = os.environ.get("ANDROID_HOME")
        if not self.android_home:
            raise Exception("For config tests ANDROID_HOME enviroment variables must be set to a valid "
                            "folder")

    def test_tool_config(self):
        # Look for javac in bin
        config.verify_java_jdk(None)
        project_config = config.ToolsConfig(None, None, None)
        assert project_config

if __name__ == '__main__':
        unittest.main()
