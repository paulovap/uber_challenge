Anteater - cli Android build system
===================================

Anteater is an Android CLI build system created as a development challenge. It allows the user to create, compile,
generate and deploy applications directly from the terminal.

Source Code
-----------

You can download the code using git:

.. code:: bash

  $ git clone https://bitbucket.org/paulovap/uber_challenge

Dependencies
------------
The project depends on Python 2.7 and setuptools module. On Windows, setuptools
is installed by the default distribution. On Linux you may
need to install additional packages. For example, on ubuntu:

.. code:: bash

    $ apt-get install python-dev python-setuptools python-pip

It also depends on Java JDK and the Android SDK properly installed.

Running from source
-------------------

You can run anteater directly from the source folder, but it is recommended
to install in your system. To run, just do:

.. code:: bash

  $ make  # it will fetch the dependencies
  $ python anteater/anteater.py -h
  usage: anteater [-h] [-v] {new,compile,pack,deploy} ...

  An Android CLI build system

  optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose         enabled verbose mode

  command:
    {new,compile,pack,deploy}
                        for more info type command --help
    new                 creates a skeleton for a new Android project
    compile             compile the source code, generating the dex files
    pack                generate the apk file and sign it with debug keystore
    deploy              deploy the final apk into a running android device or
                        emulator

Installation
------------

Make sure you have python 2.7 on your PATH environment variable and run:

.. code:: bash

    $ python setup.py install

obs: On Windows anteater will be installed in the Script folder of your python
distribution. It is generally ``C:\Python27\Scripts``, so make sure to have it
in %PATH%.

Usage
-----

The build system has four commands: ``new``, ``compile``,
``pack`` and ``deploy``.

new
  Creates a new android project from scratch. You need to pass the directory
  that the project will be created. App name, package name and target SDK can be
  passed as optional arguments.

  .. code:: bash

    usage: anteater new [-h] [--app_name APP_NAME] [--package PACKAGE]
                      [--target_sdk TARGET_SDK]
                      [directory]

    positional arguments:
      directory             name of a new directory that will be generated

    optional arguments:
      -h, --help            show this help message and exit
      --app_name APP_NAME   name that will be shown in your app (i.e SampleApp)
      --package PACKAGE     root directory of the project to be scanned (i.e.
                            org.anteater.sampleapp)
      --target_sdk TARGET_SDK
                            Android target SDK (i.e. android-23). If no target
                            provided the system will use the latest

compile
  Generates the R.java class and compile all java classes into a DEX file.

pack
  Pack the resources, libraries into an Android application(.apk), signed it
  with debug key and zipalign it.

deploy
  Install the signed apk into a active Android device or emulator.

Hooks
-----
The build system has a hook system, where custom shell can be executed before or
after a build task. For instance, you can add hooks to before ``compile``
command and after ``pack`` command in ``build.yaml`` file:

.. code:: yaml

  hooks:
    compile:
      before:
        - cmd: "ls -ll"
        - cmd: "echo \"hello world\""
    pack:
      after:
        - cmd: "ls -ll"

Limitations
-----------
Right now, it does not handle aar, assets, AIDL files.

Available Platforms
-------------------
Currently tested on Windows and Linux. Mac not tested, but should work.
